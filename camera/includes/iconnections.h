#ifndef ICONNECTIONS_H
#define ICONNECTIONS_H

class IConnection
{
  public:
  private:
};

class IWriter
{
  public:

  virtual int Write(const uint8_t* data, size_t size) = 0;

  private:
};

#endif //iconnections.h