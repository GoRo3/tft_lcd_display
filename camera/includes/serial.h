#ifndef SERIAL_H
#define SERIAL_H

#include <termios.h>
#include "iconnections.h"

using namespace std;

class SerialLcd : public IWriter
{
    public:
    explicit SerialLcd(const string& device, uint baudrate);

    int Write(const uint8_t* data, size_t size) override;
    
    ~SerialLcd();

    private:

    int m_fd;
    uint m_baudrate;
    struct termios m_termConfig;

    void initSerial();
};

#endif //SERIAL_H