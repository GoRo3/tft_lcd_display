#include <iostream>
#include <string>
#include <iomanip>
#include <sstream>
#include <vector>

#include <opencv2/core.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/video.hpp>
#include <opencv2/videoio.hpp>
#include <opencv2/highgui.hpp>

#include "iconnections.h"
#include "serial.h"

#define DEVICE "/dev/ttyUSB0"
#define SIZE_X 160
#define SIZE_Y 104

using namespace cv;
using namespace std;

uint8_t buff[(SIZE_X * SIZE_Y * 2)];

main(int argc, char const *argv[])
{

    size_t framesize = 0;

    Mat frame;
    cv::VideoCapture cap;

    SerialLcd serialDevice(DEVICE, B115200);

    cap.open(0);
    if (!cap.isOpened())
    {
        std::cout << "Nie mozna otworzyc zrodla video " << std::endl;
    }

    Size refS = Size(SIZE_X, SIZE_Y);
    cout << "Reference frame resolution: Width=" << refS.width << "  Height=" << refS.height << endl;

    for(int i =0; 1 ; i++)
    {
        cap.read(frame);

        resize(frame, frame, refS);
        cvtColor(frame, frame, COLOR_BGR2BGR565);

        if (frame.isContinuous())
        {
            framesize = (frame.total() * frame.elemSize());
            cout << "frame is continues and sie is: " << framesize << endl;

            memcpy(buff, frame.data, framesize);
        }

        cout << frame.size() << " " << frame.elemSize1() << " frame size: " << framesize << endl;

        serialDevice.Write(buff, framesize);
    }
    
    // while (cap.read(frame))
    // {

    //    // flip(frame, frame, 1);
    //     resize(frame, frame, refS);
    //     cvtColor(frame, frame, COLOR_BGR2BGR565);

    //     if (frame.isContinuous())
    //     {
    //         framesize = (frame.total() * frame.elemSize());
    //         cout << "frame is continues and sie is: " << framesize << endl;

    //         memcpy(buff, frame.data, framesize);
    //     }

    //     cout << frame.size() << " " << frame.elemSize1() << " frame size: " << framesize << endl;

    //     serialDevice.writeSerial(buff, framesize);
    // }

    return 0;
}
