#include <iostream>
#include <stdexcept>
#include <string>

#include <termios.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <string.h>
#include <sys/stat.h>

#include "serial.h"

using namespace std;

SerialLcd::SerialLcd(const string &device, uint baudrate) : m_baudrate(baudrate)
{

    m_fd = open(device.c_str(), O_RDWR | O_NOCTTY);
    if (m_fd < 0)
    {
        throw runtime_error("Can't open device to write");
    }

    cout << "Device " << device << " Has been opened" << endl;

    initSerial();
}

SerialLcd::~SerialLcd()
{
    close(m_fd);
}

int SerialLcd::Write(const uint8_t *data, size_t size)
{ 
    //sleep(2);
    tcflush(m_fd, TCIOFLUSH);

    size_t n_written = 0;
    // uint8_t synchro[3] = {123, 254, 123};

    // n_written = write(m_fd, synchro, 3);

    // sleep(2);
    tcflush(m_fd, TCIOFLUSH);

    n_written = write(m_fd, data, size);

    if (n_written < 0)
    {
        cout << "error durning writing to FD" << endl;
        return 0;
    }

    cout << "Bytes send: " << n_written << endl;

    return n_written;
}

void SerialLcd::initSerial()
{
    if (cfsetispeed(&m_termConfig, m_baudrate) != 0)
    {
        char buffer[256];
        char *errorMessage = strerror_r(errno, buffer, 256);

        throw runtime_error(buffer);
    }

    if (cfsetospeed(&m_termConfig, m_baudrate) != 0)
    {

        char buffer[256];
        char *errorMessage = strerror_r(errno, buffer, 256);

        throw runtime_error(buffer);
    }

    m_termConfig.c_oflag &= ~(ONLCR|OPOST); 
    
    m_termConfig.c_cflag &= ~(PARENB|CSTOPB|CSIZE|CRTSCTS);
    m_termConfig.c_cflag |= (CS8|CREAD|CLOCAL);
   
    m_termConfig.c_cc[VMIN] = 1;
    m_termConfig.c_cc[VTIME] = 5;

    cfmakeraw(&m_termConfig);

    tcflush(m_fd, TCIFLUSH);

    if (tcsetattr(m_fd, TCSANOW, &m_termConfig) != 0)
    {
        char buffer[256];
        char *errorMessage = strerror_r(errno, buffer, 256);

        throw runtime_error(buffer);
    }
}