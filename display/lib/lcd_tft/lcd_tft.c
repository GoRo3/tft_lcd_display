#include <freertos/FreeRTOS.h>
#include <freertos/task.h>

#include <driver/gpio.h>
#include <esp_err.h>
#include <ledc.h>
#include <rom/ets_sys.h>

#include "config.h"
#include "lcd_tft.h"
#include "AsciiLib.h"

#if USE_REGISTER == 1
#include "register.h"
#endif

#if USE_SPI == 1
#include <spi_common.h>
#include <driver/spi_master.h>
#endif //use SPI

#if USE_SPI == 1
// inline spiInit(void)
// {
//   //  ret = spi_bus_initialize();
//   return 0;
// }
#endif //use SPI

static ledc_timer_config_t ledConfig =
    {
        .duty_resolution = LEDC_TIMER_13_BIT,
        .freq_hz = 5000,
        .speed_mode = LEDC_HIGH_SPEED_MODE,
        .timer_num = LEDC_TIMER_0};

static ledc_channel_config_t ledChannel =
    {
        .channel = LEDC_CHANNEL_0,
        .duty = 8000,
        .gpio_num = LEDCTR,
        .speed_mode = LEDC_HIGH_SPEED_MODE,
        .timer_sel = LEDC_TIMER_0};

static inline void sendData(uint16_t data);
static inline void sendIndex(uint16_t index);
static inline void sendReg(uint16_t reg, uint16_t value);
static inline void gpioInit(void);

#if USE_16BIT == 1
static inline void sendFrame(uint16_t data);
static inline void gpioInput(void);
static inline uint16_t readData();
static inline uint16_t readPins();
static inline uint16_t readReg(uint16_t data);
#endif //Use 16 bit

void lcdInit(void)
{
    ledc_timer_config(&ledConfig);
    ledc_channel_config(&ledChannel);
    gpioInit();

#if USE_REGISTER == 1
    registerInit();
#endif

    RST_LOW;
    vTaskDelay(100 / portTICK_PERIOD_MS);
    RST_HIGH;

#if USE_16BIT == 1
    uint16_t deviceCode = 0;
    deviceCode = readReg(0x0000);
    printf("Reading register %#X\n", deviceCode);
#endif
    sendReg(ILI9325_Internal_Timing_2, 0x0010);
    sendReg(ILI9325_DRIVER_CODE_REG, 0x0001);
    sendReg(ILI9325_Driver_Output_Control_1, 0X0100);
    sendReg(ILI9325_LCD_Driving_Wave_Control, 0x0700);
    sendReg(ILI9325_Entry_Mode, 0x1030);
    sendReg(ILI9325_Resizing_Control_Register, 0x0000);
    sendReg(ILI9325_Display_Control_2, 0x0202);
    sendReg(ILI9325_Display_Control_3, 0x0000);
    sendReg(ILI9325_Display_Control_4, 0x0000);
    sendReg(ILI9325_RGB_Display_Interface_Control_1, 0x0000);
    sendReg(ILI9325_Frame_Maker_Position, 0x0000);
    sendReg(ILI9325_RGB_Display_Interface_Control_2, 0x0000);
    /* Power On sequence */
    sendReg(0x0010, 0x0000);
    sendReg(0x0011, 0x0000);
    sendReg(0x0012, 0x0000);
    sendReg(0x0013, 0x0000);
    vTaskDelay(20 / portTICK_PERIOD_MS);

    sendReg(ILI9325_Power_Control_1, 0x17b0);
    sendReg(ILI9325_Power_Control_2, 0x0137);

    vTaskDelay(5 / portTICK_PERIOD_MS);

    sendReg(ILI9325_Power_Control_3, 0x0139);

    vTaskDelay(5 / portTICK_PERIOD_MS);

    sendReg(ILI9325_Power_Control_3, 0x1d00);

    sendReg(ILI9325_Power_Control_7, 0x0013);

    sendReg(ILI9325_Horizontal_GRAM_Address_Set, 0X0);
    sendReg(ILI9325_Vertical_GRAM_Address_Set, 0X0);

    /********* gamma ***********************/

    sendReg(ILI9325_Gamma_Control_1, 0X7);
    sendReg(ILI9325_Gamma_Control_2, 0x0302);
    sendReg(ILI9325_Gamma_Control_3, 0x0105);
    sendReg(ILI9325_Gamma_Control_4, 0x0206);
    sendReg(ILI9325_Gamma_Control_5, 0x0808);
    sendReg(ILI9325_Gamma_Control_6, 0x0206);
    sendReg(ILI9325_Gamma_Control_7, 0x0504);
    sendReg(ILI9325_Gamma_Control_8, 0x0007);
    sendReg(ILI9325_Gamma_Control_9, 0x0105);
    sendReg(ILI9325_Gamma_Control_10, 0x0808);

    vTaskDelay(5 / portTICK_PERIOD_MS);

    /************ RAM ****************/
    sendReg(ILI9325_Horizontal_Address_Start_Position, 0x0000);
    sendReg(ILI9325_Horizontal_Address_End_Position, 0xef); //ef
    sendReg(ILI9325_Vertical_Address_Start_Position, 0x0000);
    sendReg(ILI9325_Vertical_Address_End_Position, 0x13f); //13f
    sendReg(ILI9325_Driver_Output_Control_2, 0x3700);      //a7
    sendReg(ILI9325_Base_Image_Display_Control, 0x0001);
    sendReg(ILI9325_Vertical_Scroll_Control, 0x0000);

    sendReg(0x0080, 0x0000);
    sendReg(0x0081, 0x0000);
    sendReg(0x0082, 0x0000);
    sendReg(0x0083, 0x0000);
    sendReg(0x0084, 0x0000);
    sendReg(0x0085, 0x0000);

    sendReg(0x0090, 0x0010);
    sendReg(0x0092, 0x0600);
    sendReg(0x0093, 0x0003);
    sendReg(0x0095, 0x0110);
    sendReg(0x0097, 0x0000);
    sendReg(0x0098, 0x0000); /* display on sequence */

    sendReg(0x0007, 0x0173);

    lcdSetCursor(0, 0);

    sendIndex(ILI9325_Write_Data_to_GRAM);

    vTaskDelay(5 / portTICK_PERIOD_MS);
}

void lcdChar(uint16_t x, uint16_t y, char ch, uint16_t charColor, uint16_t bgKolor)
{
    uint16_t i, j;
    uint8_t buffer[16], tmp_char;

    GetASCIICode(buffer, ch);

    for (i = 0; i < 16; i++)
    {
        tmp_char = buffer[i];
        for (j = 0; j < 8; j++)
        {
            if ((tmp_char & (1 << j)))
            {
                setPoint(x + j, y + i, charColor);
            }
            else
            {
                setPoint(x + j, y + i, bgKolor);
            }
        }
    }
}

void putText(uint16_t x, uint16_t y, char *str, uint16_t color, uint16_t bgKolor)
{
    char tempChar;

    while (*str != '\0')
    {
        tempChar = *str++;
        lcdChar(x, y, tempChar, color, bgKolor);
        if (x < MAX_X - 8)
        {
            x += 8;
        }
        else if (y < MAX_Y - 16)
        {
            x = 0;
            y += 16;
        }
        else
        {
            x = 0;
            y = 0;
        }
    }
}
void drawBox(uint16_t x, uint16_t y, uint16_t wight, uint16_t heigh, uint16_t bgcolor)
{
    lcdSetCursor(x, y);

    sendIndex(ILI9325_Write_Data_to_GRAM);

    for (int i = 0; i < heigh; i++)
    {
        for (int j = 0; j < wight; j++)
        {
            setPoint(x + j, y + i, bgcolor);
        }
    }
}
void setWindow(uint16_t x, uint16_t y, uint16_t wight, uint16_t heigh)
{
    sendReg(ILI9325_Horizontal_Address_Start_Position, y);
    sendReg(ILI9325_Horizontal_Address_End_Position, (y + heigh - 1));
    sendReg(ILI9325_Vertical_Address_Start_Position, x);
    sendReg(ILI9325_Vertical_Address_End_Position, (x + wight - 1));
}

void drawRgbstream(uint8_t *data)
{
    uint32_t index = 0, size = 0;
    uint16_t sendbuff = 0;

    sendReg(ILI9325_Entry_Mode, 0x1038);
    sendIndex(ILI9325_Write_Data_to_GRAM);

    for (index = 0; index < size * 2; index += 2)
    {
        sendbuff = ((data[index + 1] << 8) | data[index]);

        sendData(sendbuff);
    }

    sendReg(ILI9325_Entry_Mode, 0x1030);
}

// Reading 2byte ints with RGB565
void drawRgbImage(uint16_t x, uint16_t y, uint16_t Xsize, uint16_t Ysize, uint8_t *data)
{
    setWindow(x, y, Xsize, Ysize);
    uint32_t index = 0, size = 0;
    uint16_t sendbuff = 0;
    size = (Xsize * Ysize)*2;

    lcdSetCursor(x, y);

    sendReg(ILI9325_Entry_Mode, 0x1038);
    sendIndex(ILI9325_Write_Data_to_GRAM);

    for (index = 0; index < size; index += 2)
    {
        sendbuff = ((data[index + 1] << 8) | data[index]);
        sendData(sendbuff);
    }

    sendReg(ILI9325_Entry_Mode, 0x1030);
    setWindow(0, 0, MAX_X, MAX_Y);
}

void drawBitmap(uint16_t x, uint16_t y, uint8_t *data)
{
    uint32_t index = 0, size = 0;

    size = *(volatile uint16_t *)(data + 2);
    size |= (*(volatile uint16_t *)(data + 4)) << 16;

    index = *(volatile uint16_t *)(data + 10);
    index |= (*(volatile uint16_t *)(data + 12)) << 16;
    size = (size - index) / 2;
    data += index;

    sendReg(ILI9325_Entry_Mode, 0x1038);

    lcdSetCursor(x, y);

    sendIndex(ILI9325_Write_Data_to_GRAM);

    for (index = 0; index < size; index++)
    {
        sendData(*(volatile uint16_t *)data);
        data += 2;
    }

    sendReg(ILI9325_Entry_Mode, 0x1010);
}

void setPoint(uint16_t X, uint16_t Y, uint16_t point)
{
    if (X >= MAX_X || Y >= MAX_Y)
    {
        return;
    }
    lcdSetCursor(X, Y);
    sendReg(0x22, point);
}

void lcdClear(uint16_t color)
{
    uint32_t index = 0;
    lcdSetCursor(0, 0);
    CS_LOW;
    sendIndex(0x22);
    for (index = 0; index < ((MAX_X * MAX_Y)); ++index)
    {
        sendData(color);
    }
    CS_HIGH;
}
void lcdON(void)
{
    sendReg(ILI9325_Power_Control_1, 0x0);
    sendReg(ILI9325_Power_Control_2, 0x0);
    sendReg(ILI9325_Power_Control_3, 0x0);
    sendReg(ILI9325_Power_Control_4, 0x0);

    sendReg(ILI9325_Power_Control_1, 0x17B0);
    sendReg(ILI9325_Power_Control_2, 0x0137);
    sendReg(ILI9325_Power_Control_3, 0x0139);
    sendReg(ILI9325_Power_Control_4, 0x1d00);

    sendReg(ILI9325_Power_Control_7, 0x0013);

    sendReg(ILI9325_Display_Control_1, 0x0173);
}

void lcdOff(void)
{
    sendReg(ILI9325_Power_Control_1, 0x0);
    sendReg(ILI9325_Power_Control_2, 0x0);
    sendReg(ILI9325_Power_Control_3, 0x0);
    sendReg(ILI9325_Power_Control_4, 0x0);

    sendReg(ILI9325_Power_Control_7, 0x0);

    sendReg(ILI9325_Display_Control_1, 0x0);
}
void lcdSetCursor(uint16_t x, uint16_t y)
{
    sendReg(ILI9325_Horizontal_GRAM_Address_Set, y);
    sendReg(ILI9325_Vertical_GRAM_Address_Set, x);
}

void setBrightness(uint8_t duty) // 0 - 128;
{
    int brightness;

    if (duty > 128)
        brightness = 8000;
    else
        brightness = duty * 62;

    //Brightness between 100 - 8000
    ledc_set_duty(ledChannel.speed_mode, ledChannel.channel, brightness);
    ledc_update_duty(ledChannel.speed_mode, ledChannel.channel);
}

inline void sendIndex(uint16_t index)
{
    RS_LOW;
#if USE_16BIT == 1
    sendFrame(index);
#endif //use 16Bit
#if USE_REGISTER == 1
    writeToRegister(index);
#endif // use register
    WR_LOW;
    WR_HIGH;
    RS_HIGH;
}

inline void sendData(uint16_t data)
{
    RS_HIGH;
#if USE_16BIT == 1
    sendFrame(data);
#endif
#if USE_REGISTER == 1
    writeToRegister(data);
#endif
    WR_LOW;
    WR_HIGH;
}
inline void sendReg(uint16_t reg, uint16_t value)
{

    CS_LOW;
    RD_HIGH;
    sendIndex(reg);
    sendData(value);
    CS_HIGH;
}

void lcdDrawLine(uint16_t x0, uint16_t y0, uint16_t x1, uint16_t y1, uint16_t color)
{
    short dx, dy;
    short temp;

    if (x0 > x1)
    {
        temp = x1;
        x1 = x0;
        x0 = temp;
    }
    if (y0 > y1)
    {
        temp = y1;
        y1 = y0;
        y0 = temp;
    }

    dx = x1 - x0;
    dy = y1 - y0;

    if (dx == 0)
    {
        do
        {
            setPoint(x0, y0, color);
            y0++;
        } while (y1 >= y0);
        return;
    }
    if (dy == 0)
    {
        do
        {
            setPoint(x0, y0, color);
            x0++;
        } while (x1 >= x0);
        return;
    }

    /* Bresenham's line algorithm  */
    if (dx > dy)
    {
        temp = 2 * dy - dx;
        while (x0 != x1)
        {
            setPoint(x0, y0, color);
            x0++;
            if (temp > 0)
            {
                y0++;
                temp += 2 * dy - 2 * dx;
            }
            else
            {
                temp += 2 * dy;
            }
        }
        setPoint(x0, y0, color);
    }
    else
    {
        temp = 2 * dx - dy;
        while (y0 != y1)
        {
            setPoint(x0, y0, color);
            y0++;
            if (temp > 0)
            {
                x0++;
                temp += 2 * dy - 2 * dx;
            }
            else
            {
                temp += 2 * dy;
            }
        }
        setPoint(x0, y0, color);
    }
}

inline void gpioInit(void)
{

#if USE_16BIT == 1
    gpio_pad_select_gpio((D0 | D1 | D2 | D3 | D4 | D5 | D6 | D7 | D8 | D9 | D10 | D11 | D12 | D13 | D14 | D15 | RS | CS | WR | RD | RST));

    gpio_set_direction(D0, GPIO_MODE_OUTPUT);
    gpio_set_direction(D1, GPIO_MODE_OUTPUT);
    gpio_set_direction(D2, GPIO_MODE_OUTPUT);
    gpio_set_direction(D3, GPIO_MODE_OUTPUT);
    gpio_set_direction(D4, GPIO_MODE_OUTPUT);
    gpio_set_direction(D5, GPIO_MODE_OUTPUT);
    gpio_set_direction(D6, GPIO_MODE_OUTPUT);
    gpio_set_direction(D7, GPIO_MODE_OUTPUT);
    gpio_set_direction(D8, GPIO_MODE_OUTPUT);
    gpio_set_direction(D9, GPIO_MODE_OUTPUT);
    gpio_set_direction(D10, GPIO_MODE_OUTPUT);
    gpio_set_direction(D11, GPIO_MODE_OUTPUT);
    gpio_set_direction(D12, GPIO_MODE_OUTPUT);
    gpio_set_direction(D13, GPIO_MODE_OUTPUT);
    gpio_set_direction(D14, GPIO_MODE_OUTPUT);
    gpio_set_direction(D15, GPIO_MODE_OUTPUT);
// #else
//     gpio_pad_select_gpio((RS | CS | WR | RD | RST));
#endif //use 16bit

    gpio_set_direction(RS, GPIO_MODE_OUTPUT);
    gpio_set_direction(CS, GPIO_MODE_OUTPUT);
    gpio_set_direction(WR, GPIO_MODE_OUTPUT);
    gpio_set_direction(RD, GPIO_MODE_OUTPUT);
    gpio_set_direction(RST, GPIO_MODE_OUTPUT);
}

#if USE_16BIT == 1
inline void gpioInput(void)
{

    gpio_set_direction(D0, GPIO_MODE_INPUT);
    gpio_set_direction(D1, GPIO_MODE_INPUT);
    gpio_set_direction(D2, GPIO_MODE_INPUT);
    gpio_set_direction(D3, GPIO_MODE_INPUT);
    gpio_set_direction(D4, GPIO_MODE_INPUT);
    gpio_set_direction(D5, GPIO_MODE_INPUT);
    gpio_set_direction(D6, GPIO_MODE_INPUT);
    gpio_set_direction(D7, GPIO_MODE_INPUT);
    gpio_set_direction(D8, GPIO_MODE_INPUT);
    gpio_set_direction(D9, GPIO_MODE_INPUT);
    gpio_set_direction(D10, GPIO_MODE_INPUT);
    gpio_set_direction(D11, GPIO_MODE_INPUT);
    gpio_set_direction(D12, GPIO_MODE_INPUT);
    gpio_set_direction(D13, GPIO_MODE_INPUT);
    gpio_set_direction(D14, GPIO_MODE_INPUT);
    gpio_set_direction(D15, GPIO_MODE_INPUT);
}
inline uint16_t readPins()
{
    uint16_t value = 0u;

    if (gpio_get_level(D0))
    {
        value |= (1 << 0);
    }
    else
    {
        value &= ~(1 << 0);
    }
    if (gpio_get_level(D1))
    {
        value |= (1 << 1);
    }
    else
    {
        value &= ~(1 << 1);
    }
    if (gpio_get_level(D2))
    {
        value |= (1 << 2);
    }
    else
    {
        value &= ~(1 << 2);
    }
    if (gpio_get_level(D3))
    {
        value |= (1 << 3);
    }
    else
    {
        value &= ~(1 << 3);
    }
    if (gpio_get_level(D4))
    {
        value |= (1 << 4);
    }
    else
    {
        value &= ~(1 << 4);
    }
    if (gpio_get_level(D5))
    {
        value |= (1 << 5);
    }
    else
    {
        value &= ~(1 << 5);
    }
    if (gpio_get_level(D6))
    {
        value |= (1 << 6);
    }
    else
    {
        value &= ~(1 << 6);
    }
    if (gpio_get_level(D7))
    {
        value |= (1 << 7);
    }
    else
    {
        value &= ~(1 << 7);
    }
    if (gpio_get_level(D8))
    {
        value |= (1 << 8);
    }
    else
    {
        value &= ~(1 << 8);
    }
    if (gpio_get_level(D9))
    {
        value |= (1 << 9);
    }
    else
    {
        value &= ~(1 << 9);
    }
    if (gpio_get_level(D10))
    {
        value |= (1 << 10);
    }
    else
    {
        value &= ~(1 << 10);
    }
    if (gpio_get_level(D11))
    {
        value |= (1 << 11);
    }
    else
    {
        value &= ~(1 << 11);
    }
    if (gpio_get_level(D12))
    {
        value |= (1 << 12);
    }
    else
    {
        value &= ~(1 << 12);
    }
    if (gpio_get_level(D13))
    {
        value |= (1 << 13);
    }
    else
    {
        value &= ~(1 << 13);
    }
    if (gpio_get_level(D14))
    {
        value |= (1 << 14);
    }
    else
    {
        value &= ~(1 << 14);
    }
    if (gpio_get_level(D15))
    {
        value |= (1 << 15);
    }
    else
    {
        value &= ~(1 << 15);
    }

    return value;
}
uint16_t readData()
{
    uint16_t value = 0;
    RS_HIGH;
    WR_HIGH;
    RD_LOW;
    lcd_delay(0);
    value = readPins();
    lcd_delay(0);
    RD_HIGH;

    return value;
}
inline void sendFrame(uint16_t data)
{
    if (data & (1 << 0))
    {
        D0_HIGH;
    }
    else
        D0_LOW;
    if (data & (1 << 1))
    {
        D1_HIGH;
    }
    else
        D1_LOW;
    if (data & (1 << 2))
    {
        D2_HIGH;
    }
    else
        D2_LOW;
    if (data & (1 << 3))
    {
        D3_HIGH;
    }
    else
        D3_LOW;
    if (data & (1 << 4))
    {
        D4_HIGH;
    }
    else
        D4_LOW;
    if (data & (1 << 5))
    {
        D5_HIGH;
    }
    else
        D5_LOW;
    if (data & (1 << 6))
    {
        D6_HIGH;
    }
    else
        D6_LOW;
    if (data & (1 << 7))
    {
        D7_HIGH;
    }
    else
        D7_LOW;
    if (data & (1 << 8))
    {
        D8_HIGH;
    }
    else
        D8_LOW;
    if (data & (1 << 9))
    {
        D9_HIGH;
    }
    else
        D9_LOW;
    if (data & (1 << 10))
    {
        D10_HIGH;
    }
    else
        D10_LOW;
    if (data & (1 << 11))
    {
        D11_HIGH;
    }
    else
        D11_LOW;
    if (data & (1 << 12))
    {
        D12_HIGH;
    }
    else
        D12_LOW;
    if (data & (1 << 13))
    {
        D13_HIGH;
    }
    else
        D13_LOW;
    if (data & (1 << 14))
    {
        D14_HIGH;
    }
    else
        D14_LOW;
    if (data & (1 << 15))
    {
        D15_HIGH;
    }
    else
        D15_LOW;
}
uint16_t readReg(uint16_t data)
{
    uint16_t reg = 0;

    CS_LOW;
    sendIndex(data);
    gpioInput();
    reg = readData();
    CS_HIGH;

    gpio_set_direction(D0, GPIO_MODE_OUTPUT);
    gpio_set_direction(D1, GPIO_MODE_OUTPUT);
    gpio_set_direction(D2, GPIO_MODE_OUTPUT);
    gpio_set_direction(D3, GPIO_MODE_OUTPUT);
    gpio_set_direction(D4, GPIO_MODE_OUTPUT);
    gpio_set_direction(D5, GPIO_MODE_OUTPUT);
    gpio_set_direction(D6, GPIO_MODE_OUTPUT);
    gpio_set_direction(D7, GPIO_MODE_OUTPUT);
    gpio_set_direction(D8, GPIO_MODE_OUTPUT);
    gpio_set_direction(D9, GPIO_MODE_OUTPUT);
    gpio_set_direction(D10, GPIO_MODE_OUTPUT);
    gpio_set_direction(D11, GPIO_MODE_OUTPUT);
    gpio_set_direction(D12, GPIO_MODE_OUTPUT);
    gpio_set_direction(D13, GPIO_MODE_OUTPUT);
    gpio_set_direction(D14, GPIO_MODE_OUTPUT);
    gpio_set_direction(D15, GPIO_MODE_OUTPUT);

    return reg;
}

#endif // USE_16BIT