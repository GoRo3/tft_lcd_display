#ifndef LCD_TFT_H
#define LCD_TFT_H

#include <esp_err.h>
#include <gpio.h>

#include <freertos/task.h>
#include <ledc.h>


#define USE_SPI 0   //1 - YES, 0 - NO
#define USE_16BIT 0 //1 -yes, 0 - NO
#define USE_REGISTER 1 // 1 -yes, 0 - NO

#define MAX_X 320
#define MAX_Y 240

#define ILI9325 1 /* 0x9325 */

/******** PINS ***************/

#if USE_SPI == 1
#define PIN_NUM_MISO 25
#define PIN_NUM_MOSI 23
#define PIN_NUM_CLK 19
#define PIN_NUM_CS 22

#define PIN_NUM_DC 21
#define PIN_NUM_RST 18
#define PIN_NUM_BCKL 5
#endif //use SPI

#if USE_16BIT == 1

#define D0 (23)
#define D1 (22)
#define D2 (1)
#define D3 (3)
#define D4 (21)
#define D5 (18)
#define D6 (5)
#define D7 (17)
#define D8 (16)
#define D9 (4)
#define D10 (0)
#define D11 (2)
#define D12 (15)
#define D13 (33)
#define D14 (25)
#define D15 (32)
#endif //use 16bit

#define RS (22)
#define CS (23)
#define WR (21)
#define RD (16)
#define RST (25)

#define LEDCTR (26)

#if USE_16BIT == 1
#define D0_HIGH (gpio_set_level(D0, 1))
#define D0_LOW (gpio_set_level(D0, 0))
#define D1_HIGH (gpio_set_level(D1, 1))
#define D1_LOW (gpio_set_level(D1, 0))
#define D2_HIGH (gpio_set_level(D2, 1))
#define D2_LOW (gpio_set_level(D2, 0))
#define D3_HIGH (gpio_set_level(D3, 1))
#define D3_LOW (gpio_set_level(D3, 0))
#define D4_HIGH (gpio_set_level(D4, 1))
#define D4_LOW (gpio_set_level(D4, 0))
#define D5_HIGH (gpio_set_level(D5, 1))
#define D5_LOW (gpio_set_level(D5, 0))
#define D6_HIGH (gpio_set_level(D6, 1))
#define D6_LOW (gpio_set_level(D6, 0))
#define D7_HIGH (gpio_set_level(D7, 1))
#define D7_LOW (gpio_set_level(D7, 0))
#define D8_HIGH (gpio_set_level(D8, 1))
#define D8_LOW (gpio_set_level(D8, 0))
#define D9_HIGH (gpio_set_level(D9, 1))
#define D9_LOW (gpio_set_level(D9, 0))
#define D10_HIGH (gpio_set_level(D10, 1))
#define D10_LOW (gpio_set_level(D10, 0))
#define D11_HIGH (gpio_set_level(D11, 1))
#define D11_LOW (gpio_set_level(D11, 0))
#define D12_HIGH (gpio_set_level(D12, 1))
#define D12_LOW (gpio_set_level(D12, 0))
#define D13_HIGH (gpio_set_level(D13, 1))
#define D13_LOW (gpio_set_level(D13, 0))
#define D14_HIGH (gpio_set_level(D14, 1))
#define D14_LOW (gpio_set_level(D14, 0))
#define D15_HIGH (gpio_set_level(D15, 1))
#define D15_LOW (gpio_set_level(D15, 0))
#endif //use 16bit

#define RS_HIGH (gpio_set_level(RS, 1))
#define RS_LOW (gpio_set_level(RS, 0))
#define CS_HIGH (gpio_set_level(CS, 1))
#define CS_LOW (gpio_set_level(CS, 0))
#define WR_HIGH (gpio_set_level(WR, 1))
#define WR_LOW (gpio_set_level(WR, 0))
#define RD_HIGH (gpio_set_level(RD, 1))
#define RD_LOW (gpio_set_level(RD, 0))
#define RST_HIGH (gpio_set_level(RST, 1))
#define RST_LOW (gpio_set_level(RST, 0))

#define BACKLIGHT_BRIGSTNES_PIN (18)

/*******COLORS****************/

#define BLACK_RGB 0x0000
#define WHITE_RGB 0xFFFF
#define RED_RGB 0xF800
#define GREEN_RGB 0x07E0
#define BLUE_RGB 0x001F
#define YELLOW_RGB 0xFFE0
#define MAGENTA_RGB 0xF81F
#define CYAN_RGB 0x7FFF
#define GRAY_RGB 0xF7DE
#define SILVER_RGB 0xA080
#define GOLD_RGB 0xA040

/************** Global Variables **********/

esp_err_t ret;

/************** Global functions **********/

void lcdInit(void);
void lcdON(void);
void lcdOff(void);
void lcdClear(uint16_t color);

void setBrightness(uint8_t duty);
void lcdSetCursor(uint16_t x, uint16_t y);
void setWindow(uint16_t x, uint16_t y, uint16_t wight, uint16_t heigh);
void setPoint(uint16_t X, uint16_t Y, uint16_t point);

void lcdDrawLine( uint16_t x0, uint16_t y0, uint16_t x1, uint16_t y1 , uint16_t color );
void drawBox(uint16_t x, uint16_t y, uint16_t x1, uint16_t y1,uint16_t bgcolor);
void drawRgbImage(uint16_t x, uint16_t y, uint16_t Xsize, uint16_t Ysize, uint8_t *data);
void drawBitmap(uint16_t x, uint16_t y, uint8_t *data);

void putChar(uint16_t x, uint16_t y, char ch, uint16_t charColor, uint16_t bgKolor);
void putText(uint16_t x, uint16_t y, char *str, uint16_t color, uint16_t bgKolor);

#define ILI9325_DRIVER_CODE_REG 0x00
#define ILI9325_Driver_Output_Control_1 0x01
#define ILI9325_LCD_Driving_Wave_Control 0x02
#define ILI9325_Entry_Mode 0x03
#define ILI9325_Resizing_Control_Register 0x04

#define ILI9325_Display_Control_1 0x07
#define ILI9325_Display_Control_2 0x08
#define ILI9325_Display_Control_3 0x09
#define ILI9325_Display_Control_4 0x0A
#define ILI9325_RGB_Display_Interface_Control_1 0x0C
#define ILI9325_Frame_Maker_Position 0x0D
#define ILI9325_RGB_Display_Interface_Control_2 0x0F

#define ILI9325_Power_Control_1 0x10
#define ILI9325_Power_Control_2 0x11
#define ILI9325_Power_Control_3 0x12
#define ILI9325_Power_Control_4 0x13

#define ILI9325_Horizontal_GRAM_Address_Set 0x20
#define ILI9325_Vertical_GRAM_Address_Set 0x21
#define ILI9325_Write_Data_to_GRAM 0x22

#define ILI9325_Power_Control_7 0x29
#define ILI9325_Frame_Rate_and_Color_Control 0x2B

// ----------- Gamma Curve correction registers ----------//
#define ILI9325_Gamma_Control_1 0x30
#define ILI9325_Gamma_Control_2 0x31
#define ILI9325_Gamma_Control_3 0x32
#define ILI9325_Gamma_Control_4 0x35
#define ILI9325_Gamma_Control_5 0x36
#define ILI9325_Gamma_Control_6 0x37
#define ILI9325_Gamma_Control_7 0x38
#define ILI9325_Gamma_Control_8 0x39
#define ILI9325_Gamma_Control_9 0x3C
#define ILI9325_Gamma_Control_10 0x3D

//------------------ GRAM area registers ----------------//
#define ILI9325_Horizontal_Address_Start_Position 0x50
#define ILI9325_Horizontal_Address_End_Position 0x51
#define ILI9325_Vertical_Address_Start_Position 0x52
#define ILI9325_Vertical_Address_End_Position 0x53

#define ILI9325_Driver_Output_Control_2 0x60
#define ILI9325_Base_Image_Display_Control 0x61
#define ILI9325_Vertical_Scroll_Control 0x6A

//-------------- Partial Display Control register -------//
#define ILI9325_Partial_Image_1_Display_Position 0x80
#define ILI9325_Partial_Image_1_Area_Start_Line 0x81
#define ILI9325_Partial_Image_1_Area_End_Line 0x82
#define ILI9325_Partial_Image_2_Display_Position 0x83
#define ILI9325_Partial_Image_2_Area_Start_Line 0x84
#define ILI9325_Partial_Image_2_Area_End_Line 0x85

#define ILI9325_Panel_Interface_Control_1 0x90
#define ILI9325_Panel_Interface_Control_2 0x92
#define ILI9325_Panel_Interface_Control_3 0x93 // This are not documented in Datasheet V0.43
#define ILI9325_Panel_Interface_Control_4 0x95
#define ILI9325_Panel_Interface_Control_5 0x97 // This are not documented in Datasheet V0.43
#define ILI9325_Panel_Interface_Control_6 0x98 // This are not documented in Datasheet V0.43

// This are not documented in Datasheet V0.43- only in Application Notes for ILI9325 V0.22
#define ILI9325_Internal_Timing_1 0xE3
#define ILI9325_Internal_Timing_2 0xE7
#define ILI9325_Internal_Timing_3 0xEF

#endif