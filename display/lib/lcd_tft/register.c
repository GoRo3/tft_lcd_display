#include <freertos/FreeRTOS.h>
#include <freertos/task.h>
#include <gpio.h>

#define SRCLK (17)
#define SER (19)
#define RCLK (18)

void writeToRegister(uint16_t value)
{
    gpio_set_level(SRCLK, 0);
    gpio_set_level(RCLK, 0);

    for (int i = 15; i >= 0; i--)
    {
        if ((value & (1 << i)))
        {
            gpio_set_level(SER, 1);
        }
        else
        {
            gpio_set_level(SER, 0);
        }
        gpio_set_level(SRCLK, 1);
        gpio_set_level(SRCLK, 0);
    }

    gpio_set_level(RCLK, 1);
}
void registerInit()
{   
    gpio_set_direction(SRCLK, GPIO_MODE_OUTPUT);
    gpio_set_direction(SER, GPIO_MODE_OUTPUT);
    gpio_set_direction(RCLK, GPIO_MODE_OUTPUT);
    gpio_set_level(SRCLK, 0);
    gpio_set_level(SER, 0);
    gpio_set_level(RCLK, 0);
}