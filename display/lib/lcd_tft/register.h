
#ifndef REGISTER_H
#define REGISTER_H

#include <freertos/FreeRTOS.h>

void registerInit();
void writeToRegister(uint16_t value);

#endif 