#include <freertos/FreeRTOS.h>
#include <freertos/task.h>
#include <driver/uart.h>
#include <freertos/event_groups.h>
#include <esp_log.h>
#include <string.h>
#include <gpio.h>

#include <esp_system.h>
#include <esp_wifi.h>
#include <esp_event_loop.h>
#include <esp_log.h>
#include <nvs_flash.h>

#include <lwip/err.h>
#include <lwip/sys.h>
#include <lwip/sockets.h>
#include <lwip/netdb.h>

#include "lcd_tft.h"
#include "picture.h"
#include "wifi.h"
#include "udp.h"

#define SYNCHRO_BITS 3
#define UART_TRANSFER_COMPLET (1 << 0)

#define BAUDRATE 115200

static uint8_t data[34000];

static EventGroupHandle_t uartComGroup;

const char *INIT_TAG = "Init sequence: ";
const char *UPP_TAG = "UDP Socket";
const char *IPV4_TAG = "ipv4: ";

int IPV4_GITIP_BIT = BIT0;

void uartTask(void *param)
{
    static const uart_config_t uartconf =
        {
            .baud_rate = BAUDRATE,
            .data_bits = UART_DATA_8_BITS,
            .parity = UART_PARITY_DISABLE,
            .stop_bits = UART_STOP_BITS_1,
            .flow_ctrl = UART_HW_FLOWCTRL_DISABLE,
        };

    size_t lenght = 0, receivedBytes = 0, framePtr = 0;
    uint8_t *dataPtr = param;

    uart_param_config(UART_NUM_0, &uartconf);
    uart_set_baudrate(UART_NUM_0, BAUDRATE);
    uart_set_pin(UART_NUM_0, 1, 3, UART_PIN_NO_CHANGE, UART_PIN_NO_CHANGE);
    uart_driver_install(UART_NUM_0, 1024, 0, 10, NULL, 0);

    static const char *RX_TASK_TAG = "RX_TASK";
    esp_log_level_set(RX_TASK_TAG, ESP_LOG_INFO);

    printf("Initializing UART witk FIFO: %d, and max bitrate: %d", UART_FIFO_LEN, UART_BITRATE_MAX);

    while (1)
    {
        ESP_ERROR_CHECK(uart_get_buffered_data_len(UART_NUM_0, (size_t *)&lenght));

        lenght = uart_read_bytes(UART_NUM_0, &dataPtr[framePtr], lenght, 100 / portTICK_RATE_MS);

        receivedBytes += lenght;
        framePtr = receivedBytes;

        if (receivedBytes > 33280)
        {
            ESP_LOGI(RX_TASK_TAG, "Read %d bytes ", receivedBytes);
            receivedBytes = 0;
            framePtr = 0;
            lenght = 0;
            uart_flush(UART_NUM_0);
            xEventGroupSetBits(uartComGroup, UART_TRANSFER_COMPLET);
        }
        if (lenght > 0)
        {
            lenght = 0;
            uart_flush(UART_NUM_0);
        }
    }
}

void printImgTask(void *param)
{
    static const char *PRINT_TAG = "Print Task";
    esp_log_level_set(PRINT_TAG, ESP_LOG_INFO);

    while (1)
    {
        xEventGroupWaitBits(uartComGroup, UART_TRANSFER_COMPLET, 1, 0, portMAX_DELAY);
        ESP_LOGI(PRINT_TAG, "Printing IMAGE. ");
        drawRgbImage(30, 40, 160, 104, param);
    }
}

void app_main(void)
{
    nvs_flash_init();
    
    printf("Start ...... \n");

    wifiInit();
    ESP_LOGI(INIT_TAG, "ESP_WIFI_MODE_AP");

    lcdInit();
    setBrightness(128);

    lcdClear(WHITE_RGB);

    drawRgbImage(0, 0, 320, 208, (uint8_t *)picture);

    uartComGroup = xEventGroupCreate();
    xEventGroupClearBits(uartComGroup, 0xff);

    xTaskCreate(&uartTask, "Uart Comunication", 2048, data, 12, NULL);
    xTaskCreate(&printImgTask, "Print Task", 1024, data, 12, NULL);
    xTaskCreate(&UDP_task, "UDP_task", 4096, NULL, 5, NULL);
}
