#include <freertos/FreeRTOS.h>
#include <freertos/task.h>
#include <freertos/event_groups.h>
#include <esp_log.h>
#include <string.h>
#include <gpio.h>

#include <esp_system.h>
#include <esp_wifi.h>
#include <esp_event_loop.h>
#include <esp_log.h>

#include <lwip/err.h>
#include <lwip/sys.h>
#include <lwip/sockets.h>
#include <lwip/netdb.h>


#include "udp.h"
#include "wifi.h"


int udp_ipv4_socket()
{

    int sock = -1;

    sock = socket(AF_INET, SOCK_DGRAM, IPPROTO_IP);
    if (sock < 0)
    {
        ESP_LOGE(IPV4_TAG, "Failed to create socket. Error %d", errno);
        return -1;
    }

    saddr.sin_family = AF_INET;
    saddr.sin_addr.s_addr = htonl(INADDR_ANY);
    saddr.sin_port = htons(UDP_PORT);

    int err = bind(sock, (struct sockaddr *)&saddr, sizeof(struct sockaddr_in));
    if (err < 0)
    {
        ESP_LOGE(IPV4_TAG, "Failed to bing socket. Error %d", errno);
        close(sock);
        return -1;
    }

    return sock;
}

void UDP_task(void *pvParameters)
{
    char dataBuffer[128];
    int revc_data;
    uint32_t bits = 0;

    bits |= IPV4_GITIP_BIT;

    ESP_LOGI(UPP_TAG, "Waiting for connection...");
    xEventGroupWaitBits(wifiEventGroup, bits, false, true, portMAX_DELAY);
    ESP_LOGI(UPP_TAG, "Client connected!!!");
    ESP_LOGI(UPP_TAG, "WAITING FOR incoming Data");

    int sock = udp_ipv4_socket();
    if (sock < 0)
    {
        ESP_LOGE(UPP_TAG, "Failed to create IPv4 socket");
    }

    socklen = sizeof(remoteAddr);
    memset(dataBuffer, 0, 128);

    revc_data = recvfrom(sock, dataBuffer, 128, 0, (struct sockaddr *)&remoteAddr, &socklen);

    if (revc_data > 0)
    {
        ESP_LOGI(UPP_TAG, "Recv data!\n");
        ESP_LOGI(UPP_TAG, "transfer data with %s:%u\n", inet_ntoa(remoteAddr.sin_addr), ntohs(remoteAddr.sin_port));
    }
    
    else
    {
        close(sock);
    }

    vTaskDelay(500 / portTICK_RATE_MS);

    while (1)
    {
        revc_data = recvfrom(sock, dataBuffer, 128, 0, (struct sockaddr *)&remoteAddr, &socklen);

        if (revc_data > 0)
        {
            ESP_LOGE(UPP_TAG, "Received Data on UDP port: %s", dataBuffer);
        }

        ESP_LOGI(UPP_TAG, "Received packet from");

        dataBuffer[revc_data] = '\0';
    }

    close(sock);
}