#ifndef UDP_H
#define UDP_h

#include <freertos/FreeRTOS.h>
#include <freertos/task.h>
#include <freertos/event_groups.h>
#include <esp_log.h>
#include <string.h>
#include <gpio.h>

#include <esp_system.h>
#include <esp_wifi.h>
#include <esp_event_loop.h>
#include <esp_log.h>

#include <lwip/err.h>
#include <lwip/sys.h>
#include <lwip/sockets.h>
#include <lwip/netdb.h>

#include "wifi.h"

#define UDP_PORT 12345
#define IPV4 "192.168.4.5"

struct sockaddr_in saddr, remoteAddr;
unsigned int socklen;

extern const char *UPP_TAG;
extern const char *IPV4_TAG;

int udp_ipv4_socket();
void UDP_task(void *pvParameters);


#endif //UDP_H