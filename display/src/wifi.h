#ifndef WIFI_H
#define WIFI_H

#include <freertos/FreeRTOS.h>
#include <esp_log.h>
#include <string.h>
#include <gpio.h>

#include <esp_system.h>
#include <esp_wifi.h>
#include <esp_event_loop.h>
#include <esp_log.h>
#include <nvs_flash.h>

#include <lwip/err.h>
#include <lwip/sys.h>
#include <lwip/sockets.h>
#include <lwip/netdb.h>

#define WIFI_SSID "esp_test"
#define WIFI_PASS "123erozja"
#define ESP_MAX_RETRY 5

extern int IPV4_GITIP_BIT;

EventGroupHandle_t wifiEventGroup;

void wifiInit(void);

#endif //wifi